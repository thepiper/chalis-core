# chalis-core

chalis is a too-big dream of a linux-native OneNote alternative with good handwriting support.

status: shaving the hairiest yak.

# proposed architecture

A page handling core written in rust that works with SVG/XML notebooks, page rendering done by leveraging cairo, while the window dressing will be turned into a bunch of heirarchical whatevers and then spat out into native widget code in the big three desktops.

Android app is in the works, but working with android is not appealing, will probably use the same engine - hence chalis-core.